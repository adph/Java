package com.delpozo.alberto;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int opcion;

        ArrayList<Producto> productos = new ArrayList<>();

        productos.add(new Producto(1, "producto 1", 10.5, 1000, 'T'));
        productos.add(new Producto(2, "producto 2", 50.5, 2000, 'R'));
        productos.add(new Producto(3, "producto 3", 10.5, 1500, 'T'));
        productos.add(new Producto(4, "producto 4", 30.5, 2000, 'B'));
        productos.add(new Producto(5, "producto 5", 20.5, 3000, 'T'));

        do {

            opcion = Integer.parseInt(JOptionPane.showInputDialog("Menu\n" +
                    "1. Escribir\n" +
                    "2. Leer\n" +
                    "Pulsa una opcion:"));
        } while (opcion < 1 || opcion > 2);

        if (opcion == 1) escrituraRandomAccessFile(productos);
        else lecturaRandomAccessFile();

    }

    public static void escrituraRandomAccessFile(ArrayList<Producto> productos) {

        try (RandomAccessFile raf = new RandomAccessFile("ejemplo_raf.dat", "rw")) {

            for (Producto p : productos) {
                raf.writeInt(p.getId());

                /*limitamos es String a un tama;o de caracteres. En este caso a 10
                Si pone mas de 10 caracteres se corta y si pone menos le pone espacios
                 */

                StringBuffer sb = new StringBuffer(p.getNombre());
                sb.setLength(10);
                raf.writeChars(sb.toString());

                raf.writeDouble(p.getPrecio());
                raf.writeLong(p.getSueldo());
                raf.writeChar(p.getTipo());
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void lecturaRandomAccessFile() {

        Scanner scan = new Scanner(System.in);
        int id;

        /*int 4
        String los caracteres que le hayamos reservado * 2
        double 8 , long 8, char 2
         */
        //   .set
        try (RandomAccessFile raf = new RandomAccessFile("ejemplo_raf.dat", "rw")) {

            do {
                System.out.print("Introduce id del producto (entre 1 y 5): ");
                id = scan.nextInt();

                raf.seek(0); //posicionamos el punteroal principio del fichero.

                long tamanoRegitroRaf = Integer.BYTES + Character.BYTES * 10 + Double.BYTES + Long.BYTES + Character.BYTES;

                switch (id) {
                    case 1 -> raf.seek(0);
                    case 2 -> raf.seek(tamanoRegitroRaf);
                    case 3 -> raf.seek(tamanoRegitroRaf * 2);
                    case 4 -> raf.seek(tamanoRegitroRaf * 3);
                    case 5 -> raf.seek(tamanoRegitroRaf * 4);
                    default -> System.out.println("Opcion incorecta");
                }
            } while (id < 1 || id > 5);

            System.out.println(raf.readInt());
            String nombre = "";
            for (int i = 0; i < 10; i++) {
                nombre += raf.readChar();
            }
            System.out.println(nombre);
            System.out.println(raf.readDouble());
            System.out.println(raf.readLong());
            System.out.println(raf.readChar());


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
