package com.delpozo.alberto;

public class Producto {
    private int id;
    private String nombre;
    private double precio;
    private long sueldo;
    private char tipo;

    public Producto(int id, String nombre, double precio, long sueldo, char tipo) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.sueldo = sueldo;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public long getSueldo() {
        return sueldo;
    }

    public void setSueldo(long sueldo) {
        this.sueldo = sueldo;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
}
